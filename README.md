# RefillSDKFramework

## Requirements

Minimum Deployment Target: 16
Swift Version: 5.0+

## Installation

RefillSDKFramework is available through Swift Package Manager.
To install it manually just clone the repo URL, insert it into the Xcode package dependencies and choose "RefillSDKFramework".

## Usage

The app will need to add `import RefillSDK` to import it into code and use as needed. Some other considerations are documented below:

### Info.plist Entries and Config
Even though all properties can be passed dinamically when constructing `SDKConfig`, if those are not provided, config will look for corresponding properties in plist:
```xml
<key>SRAPIKey</key>
<string>{api_key}</string>
<key>SRCustomerOwner</key>
<string>{company_name}</string>
<key>SREnvironment</key>
<string>{environment}</string>
<key>SROutputLog</key>
<true/>
```

If any Web portal (Payment portal, Sim Registration portal) or Auth OTP link handling is to be used, CFBundleURLTypes are not optional and have to be defined for schemes that match `UrlCallbackConfig.paymentPortalRedirectURL`, `UrlCallbackConfig.simRegPortalRedirectURL`, `UrlCallbackConfig.swishRedirectURL` and `UrlCallbackConfig.authOtpRedirectURL`. 
See `SDKConfig.UrlCallbackConfig` for more info. By default `SDKConfig.UrlCallbackConfig` will try to retrieve values matching identifiers below.
```xml
<key>CFBundleURLTypes</key>
<array>
    <dict>
        <key>CFBundleURLName</key>
        <string>SRPaymentPortalRedirectURL</string>
        <key>CFBundleURLSchemes</key>
        <array>
            <string>{paymentPortalRedirectURL}</string>
        </array>
    </dict>
    <dict>
        <key>CFBundleURLName</key>
        <string>SRSimRegistrationPortalRedirectURL</string>
        <key>CFBundleURLSchemes</key>
        <array>
            <string>{simRegPortalRedirectURL}</string>
        </array>
    </dict>
    <dict>
        <key>CFBundleURLName</key>
        <key>Swish</key>
        <key>CFBundleURLSchemes</key>
        <array>
            <string>{swishRedirectURL}</string>
        </array>
    </dict>
        <dict>
        <key>CFBundleURLName</key>
        <key>SRAuthOtpRedirectURL</key>
        <key>CFBundleURLSchemes</key>
        <array>
            <string>{authOtpRedirectURL}</string>
        </array>
    </dict>
</array>
```

### Configure SDK on launch.
To init SDK, you should add the following at app launch:

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    RefillSDKApp.configure(SDKConfig())
    return true
}
```

Or for SwiftUI

```swift
@main
struct SDKReferenceAppSwiftUIApp: App {    
    init() {
        RefillSDKApp.configure(SDKConfig())
    }
```

Configure SDKConfig by overriding any of its properties AND/OR adding correct keys to the Info.plist. 


### Project Structure
    - ContentKit
        - Repository
        - ContentService
    - ProductKit
        - Repository
        - ProductService
    - RefillKit
        - Repository
        - AuthenticationService
        - CardRegistrationService
        - CustomerService
        - AnonymousSubscriptionService
        - AnonymousOrderService
        - SubscriptionService
        - OrderService
        - PhoneService
        - AgreementService
        - ReceiptService
        - RewardService
        - FeatureTogglesService
    - SimRegistrationKit
        - Repository
        - SimRegistrationService

        
To access any of the services or repositories, they can be used as this example: 
    * RefillSDKApp.refillKit.rewardService
    * RefillSDKApp.refillKit.repository
    * RefillSDKApp.simRegistrationKit.simRegistrationService
    
Use `.cmsLocalized` extension on the String class to fetch the localized string for a Content Kit text key. This will always return a string localized to the language `RefillSDK` is configured with. A language can be explicitly passed to fetch localized string for that language using `.cmsLocalized(language:)` method on String class.

As a user of this framework you can simplify the usage by creating own variables/references to kits like this in a seperate swift file: 
    
```swift
let refillKit = RefillSDKApp.shared.refillKit

// And then in the app use the services like this: 

let orderService = refillKit.orderService
```

### Loading Catalogs
Use the `ProductService` class's `getCatalogs()` method to load the catalogs:

```swift
let ps = RefillSDKApp.shared.productKit.productService
ps.getCatalogs() { catalogs, error, code in
...
}
```
Please have a look at the `Catalog`, `Group` and `Product` classes to see available properties and convinience methods available to easily navigate and find elements in a given catalog.

### Orders and Subscriptions
The SDK provides `OrderService` , `AnonymousOrderService` and similiar classes for Orders within `RefillSDKApp.shared.refillKit` that allows you to perform an Order / Subscription. It takes an instance of `OrderRequest` or  `SubscriptionRequest` as input and at the end returns `OrderStatus` or `SubscriptionStatus` object on success or a `NetworkError` on failure. Use the `NetworkError.localizedDescription` method to get the error message. These service classes makes use of a webapp (Payment Portal) to process the payment. Please make sure to add the following URLTypes to your project:

- `UrlCallbackConfig.paymentPortalRedirectURL`: Payment Portal will use this schema to communicate success or failure of the transaction to the app so please make sure this is a unique URL that is only used by your app to communicate with Payment Portal.
- `UrlCallbackConfig.swishRedirectURL`: Payment Portal will use this schema to communicate success/failure back to the app from Swish payment flow.
- `UrlCallbackConfig.urlLaunchMethod`: define what method should be used to launch portal: `embedded` will launch portal using embedded browser, `external` will use default external browser.

Please make sure to use different values for these redirects. In case your app supports multiple variants (Test/Staging/Prod etc.), please make sure to have different values for above schema's for each target so that the correct variant gets the callback when more than 1 variant is installed on a test device. The app will receive either of these redirects in AppDelegate so please don't forget to add the following to your AppDelegate:

```swift
func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
    RefillSDKApp.shared.processUrl(url)
}
```

Or within the rootView in the App class in a SwiftUI project
```swift
    NavigationView {
        ExampleRootView()
            .onOpenURL { url in
                print("Handling portal callback url: \(url)")
                RefillSDKApp.shared.processUrl(url)
}
```
The payment process will not be able to finish if this trigger is not passed onto the SDK. This also means that there can only be 1 active transaction in process at the same time. If another transaction is triggerd with in the app, the SDK will loose the previous state and start tracking the new transaction instead and feedback to the user will be lost as a result.

### Repositories
`RefillRepository`, `SimRegistrationRepository`, `ProductRepository` and `ContentRepository` are accessible via their respective Kits. Repositories contain cached relevant data associated with the services' calls. Repositories conform to `ObservableObject` protocol and is the data variables are `@Published` so the data is state-aware in SwiftUI - meaning that results of service calls can most of the time be ignored as UI can rely on repository data alone.

#### Testing Swish
Swish does not have a test app and as a result the Swish app will show an error when a transaction is attempted in Test/Staging environment. You will have to simulate the callback from Swish to continue the transaction. One way can be by adding the following code to AppDelegate and then setting `isTestingSwish` to true before starting the transaction:

```swift
class AppDelegate: UIResponder, UIApplicationDelegate {

    var isTestingSwish = false
    ...
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if let swishRedirect = URL(string: "my-app-swish:"), isTestingSwish {
            isTestingSwish = false
            RefillSDKApp.shared.processUrl(url: swishRedirect)
        }
    }
```

Variable isTestingSwish can be used similiarly in a SwiftUI Project but declared with a @State modifier. You can also  further tie the `isTestingSwish` to your project settings so that it's set in Test/Staging variants but never included in prod variant. However, since each app handles things differently, this is entirely a suggestion and left up to each app. 

### Sim Registration 
The SDK provides `SimRegistrationService` that allows you to perform a Sim Registraton and also check the status of Sim Registration. It takes an instance of `String` (phoneNumber) as input and at the end returns `SimRegStatus` object on success or a `NetworkError` on failure. Use the `NetworkError.localizedDescription` method to get the error message. This sim registration service class makes use of a webapp (Sim Registration Portal) to process the sim registration. Please make sure to add the following URLTypes to your project:

- `UrlCallbackConfig.simRegPortalRedirectURL`: Sim Registration Portal will use this schema to communicate success or failure of the transaction to the app so please make sure this is a unique URL that is only used by your app to communicate with Payment Portal.
- `UrlCallbackConfig.urlLaunchMethod`: See "Orders and Subscriptions" 

Make sure the correct callback is being triggered. This should be done also for any other SDKs/APIs that are being used that needs a callback. Example: 

```swift
    NavigationView {
        ExampleRootView()
            .onOpenURL { url in
                RefillSDKApp.shared.processUrl(url)
            }
    }
```

## License

RefillSDKFramework is properiety and can not be used with out explicit permission from Smart Refill AB
