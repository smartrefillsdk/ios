#!/usr/bin/env bash

# Causes the script to stop if any command fails
set -e

if [ -z "$1" ]
then
	echo "Please provide the type of release {patch/minor/major}"
	exit 1
fi

if [ -z "$2" ]
then
	echo "Please provide path to local repo of RefillSDK"
	exit 1
fi

podPath=$(pwd)
frameworkName="RefillSDK.xcframework"
xcframeworkPath="$2/archive/$frameworkName"

echo "Moving to $2 to build framework for release"
cd $2

echo "Trigger build_for_release script"
"$2/build_for_release.sh" $1

echo "Moving back to pod folder"
cd $podPath

echo "Removing older framework version"
rm -rf $frameworkName

echo "Copying the new framework version"
cp -R $xcframeworkPath ./$frameworkName

echo "Trigger release script for pod"
case "$1" in
	patch) 
		bundle exec fastlane patch_release
		;;
	minor) 
		bundle exec fastlane minor_release 
		;;
	major) 
		bundle exec fastlane major_release 
		;;
	*) 
		echo "Invalid release type specified. Must be one of patch/minor/major"
		exit 1 
		;;
esac
