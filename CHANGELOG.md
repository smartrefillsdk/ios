# CHANGELOG

## 2.1.1 (2025-02-17)
* Fix use correct customer response when creating account using OTP

## 2.1.0 (2025-02-13)
* Improve error message handling under the hood.

## 2.0.0 (2024-12-20)
* Adds support for email authentication. Added methods to `AuthenticationService` with support to login, register account and verify email using OTP. 
* Add convenience extension methods `Bucket[].getBucketWith(type)`, `Bucket[].getBucketWith(name)`, `Bucket[].getSumOfKbFromAllBuckets`, `Customer.hasSubscriptions`, `Customer.hasMigratedToOtpAuth` `Group[].hasAnySubscriptions`, `Group[].hasAllSubscriptions`
* Add method `RefillSDKApp.processUrl` that replaces the need to manually call `RefillKit.paymentPortalHelper.processPaymentResult` and `SimRegistrationKit.simRegistrationPortalHelper.processSimRegistrationResult` (which are no longer public)
* Add `OrderStatus.getErrorMessageFromRefillStatus`. Allows us to provide better error messages for orders that are not successful.
* Add localizations tor `NetworkError`. Use `NetworkError.localizedMessage` to always get user-firendly presentable message and `NetworkError.developerMessage` for more technical description of the error.
* Rename `WebPortalConfig` to `UrlCallbackConfig`
* Change minimum iOS version is now 15.0 (down from 16.0)

## 1.2.2 (2024-09-24)
* Fix: Emit changes to repositories manually when applicable to ensure ObservableObject knows when its data updates.
* Fix: Handle cases where property boolean is parsed from upper or mixed-case string (eg. "True", "true", "TRUE")
* Fix: Properties type related to bonus point changed to `Double` from `Int16` for `actualAmount` and `originalAmount`
* Change properties related to bonus points in `Customer` optional.

## 1.2.1 (2024-05-27)
* Fix issue where sim registration url was being read incorrectly.

## 1.2.0 (2024-05-24)
* Changed `RefillSDKApp.configure`, it now requires `SDKConfig`. Usage of `plist` propertes is now optional. See documentation from `SDKConfig` for all further info.
* Fix issue where wrong callback was passed to Payment portal when paying with Swish.

## 1.1.1 (2024-02-16)
* Add `SimRegistrationRepositoru.removeStatus(phoneNumber)` method.
* Change `SimRegistrationRepository.setSimRegistrationStatus` method visibility to internal.
* Change `SimRegistrationRepository.getStatusesNeedRegistration` now inclde stateses with `.futureIdentificationRequired` in addition to `.immediateIdentificationRequired`

## 1.1.0 (2024-02-02)
* Evaluating login can now be done either by `RefillRepository.customer.isLoggedIn` or checking stored credentials with `RefillRepository.hasPersistedSessionToken()`.
* Add `RefillKit.repository`, which persists information about successful media and texts retrievals. 
* Add option to choose portal launch method by setting a custom info property `SRPortalURLLaunchMethod` with value `embedded` or `external`
* Add helper methods to sdk: `countRunningRequests()` and `cancelRunningRequests()`
* Add helper extension methods for debug mode: `RefillRepository.forPreviews()` and `ProductRepository.forPreviews()` return repository instance with existing data.
* Add helper extensions to `ProductKit` models and their arrays.
* Rename `CMSLanguage` to simply `LanguageConfig`
* Change `Event.created` and `Voucher.created` are now of type `Date` instead of `String`.
* Change `CardRegistrationService.registerCard` and `CardRegistrationService.removeCard` completion signature - it now implicitly retrieves and returns latest state of the `Customer`, since those methods change it on the refill side.
* Change `CustomerService.delete` `RequestCompletionHandler` contains `String` message instead of `Bool`
* Change `Phone.accountExpirationDate`is now public and `Date` instead of `String`.
* Fix authenticated calls resulting in `NetworkError.unauthorized` will now invoke callback block (before, only notification was posted).
* Fix `AuthenticationTokenExpired` notification from `NotificationCenter` now passes `NetworkError.unauthorized` instead of `nil`
* Fix multiple service calls now have default callback value set to `nil` to ommit redundant code in case we're not interested in results.

## 1.0.5 (2023-10-17)
* Add option to include failed orders in `PhoneService.getPhonesForCustomer` and `PhoneService.getPhone`.
* Make the `AgreementService` more explicit by dividing `AgreementService.get` into `AgreementService.getAgreements` and `AgreementService.getAgreementsForCustomer`.
* Fix bug where products were sorted by order descending instead of ascending.

## 1.0.4 (2023-08-02)
* Fix issue with text not loading if `String.cmsLocalized` was called before any texts were present yet.
* Add feature to `ProductRepository` to store catalogs mapped to a phone number.

## 1.0.3 (2023-06-26)
* Add `paymentVerified` property to `Subscription`
* Fix `OrderService.performRewardOrder` requires authentication
* Fix rename `Òrder.priceplanEntryName` to `Òrder.pricePlanEntryName` as backend property is named this way
* Fix all authenticated calls in `RefillKit` fully handle session state on `Error.unauthorized`

## 1.0.2 (2023-05-04)
* Add `validatePassword` to `AuthenticationService`

## 1.0.1 (2023-03-23)
* Fix explicitly define `ObservableObject` as `Combine.ObservableObject` to avoid packaging conflicts
  
## 1.0.0 (2023-02-23)  
* Initial public release. Match structure and naming between all platforms.
